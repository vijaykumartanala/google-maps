//
//  ViewController.swift
//  iOSTask
//
//  Created by Shivaji Yerra on 22/08/18.
//  Copyright © 2018 Shivaji Yerra. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GoogleMaps
import CoreLocation

class ViewController: UIViewController,GMSPlacePickerViewControllerDelegate,CLLocationManagerDelegate,GMSMapViewDelegate {
    var googlePlace: GMSPlace? = nil
    var mapView:GMSMapView?
    var locationManager = CLLocationManager()
    var marker = GMSMarker()
    var lat  = ""
    var long = ""
    var address : String? = nil
    
    lazy var placePicker: GMSPlacePickerViewController = {
        let config = GMSPlacePickerConfig(viewport: nil)
        let picker = GMSPlacePickerViewController(config: config)
        picker.delegate = self
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       locationService()
        
    }

    //MARK:- custom Methods
    func getCurrentLocation() {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 15)
        if mapView == nil {
            mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.height), camera: camera)
            mapView?.center = self.view.center
            mapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            mapView?.isMyLocationEnabled = true
            mapView?.delegate = self
            self.view = mapView
        }
        mapView?.camera = camera
        showMarker(position: camera.target)
    }
    
    func locationService() {
        // If location services is enabled get the users location
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 50
            locationManager.allowsBackgroundLocationUpdates = true
        }
    }
    
    func showMarker(position: CLLocationCoordinate2D) {
        marker.position = position
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.title = address
        marker.map = mapView
        self.navigationItem.title = address ?? "Location...."
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        present(placePicker, animated: true, completion: {
        })
    }
    
    // MARK:- CLLocationManagerDelegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async {
            if let location = locations.first {
                self.lat = String(location.coordinate.latitude)
                self.long = String(location.coordinate.longitude)
                let geoCoder = CLGeocoder()
                let location = CLLocation(latitude:location.coordinate.latitude, longitude: location.coordinate.longitude)
                geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                    // Place details
                    if let placeMark = placemarks?.first
                    {
                        self.address = "\(placeMark.subLocality ?? ""),\(placeMark.locality ?? ""),\(placeMark.country ?? "")"
                        self.navigationItem.title = self.address ?? "Location...."
                    }
                    
                })
            }
            self.getCurrentLocation()
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    //MARK:- GMSMapViewDelegate Methods
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: {
            self.googlePlace = place
            self.lat = String(place.coordinate.latitude)
            self.long = String(place.coordinate.longitude)
            self.address = place.formattedAddress
            self.getCurrentLocation()
        })
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
}

